require('./sass/reset.scss');
require('./sass/mixins.scss');
require('./sass/main.scss');
require('./sass/header.scss');
require('./sass/info.scss');
require('./sass/sales.scss');
require('./sass/form.scss');
require('./sass/footer.scss');
require('./sass/statistic.scss');
require('./sass/brands.scss');
require('./sass/extra.scss');
require('./sass/popups.scss');
