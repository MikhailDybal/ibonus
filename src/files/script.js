new WOW().init();

// -----extras carousel-------
function runTimer() {
  iBonus.timer = setInterval(function() {
    $(".extra-quote").toggleClass("active");
    $(".extra-screen").toggleClass("active");
  }, 5000);
}

function stopTimer() {
  clearInterval(iBonus.timer);
}

runTimer();

function setActiveSlide1() {
  $(".extra-quote-2").removeClass("active");
  $(".extra-screen-2").removeClass("active");
  $(".extra-screen-1").addClass("active");
  $(".extra-quote-1").addClass("active");
  stopTimer();
}

function setActiveSlide2() {
  $(".extra-quote-1").removeClass("active");
  $(".extra-screen-1").removeClass("active");
  $(".extra-screen-2").addClass("active");
  $(".extra-quote-2").addClass("active");
  stopTimer();
}

function toogleActiveSlide() {
  if ($('.extra-quote-1').hasClass('active')) {
    setActiveSlide2();
  } else {
    setActiveSlide1();
  }
}

$(".extra-quote-1").click(function() {
  setActiveSlide1();
});

$(".extra-quote-2").click(function() {
  setActiveSlide2();
});

$('.extra').on('swipeleft',function (e,data){
  toogleActiveSlide();
});

$('.extra').on('swiperight',function (e,data){
  toogleActiveSlide();
});

$(".extra-quote-1").mouseenter(function() {
  setActiveSlide1();
});

$(".extra-quote-2").mouseenter(function() {
  setActiveSlide2();
});

$(".extra-content").mouseenter(function() {
  stopTimer();
});

$(".extra-content").mouseleave(function() {
  runTimer();
});

// !-----extras carousel-------

// -----forms behaviour-----
var getUrlParameter = function getUrlParameter(sParam) {
  var sPageURL = window.location.search.substring(1),
    sURLVariables = sPageURL.split("&"),
    sParameterName,
    i;

  for (i = 0; i < sURLVariables.length; i++) {
    sParameterName = sURLVariables[i].split("=");

    if (sParameterName[0] === sParam) {
      return sParameterName[1] === undefined
        ? true
        : decodeURIComponent(sParameterName[1]);
    }
  }
};

getUrlParameter("utm_source");
iBonus.status = getUrlParameter("status") || iBonus.status;
iBonus.utm_source = getUrlParameter("utm_source") || iBonus.utm_source;
iBonus.utm_medium = getUrlParameter("utm_medium") || iBonus.utm_medium;
iBonus.utm_campaign = getUrlParameter("utm_campaign") || iBonus.utm_campaign;
iBonus.utm_term = getUrlParameter("utm_term") || iBonus.utm_term;
iBonus.utm_content = getUrlParameter("utm_content") || iBonus.utm_content;
iBonus.after_success_url = getUrlParameter("after_success_url") || iBonus.after_success_url;
iBonus.after_failure_url = getUrlParameter("after_failure_url") || iBonus.after_failure_url;

if (iBonus.utm_source) $("input[name=utm_source]").val(iBonus.utm_source);
if (iBonus.utm_medium) $("input[name=utm_medium]").val(iBonus.utm_medium);
if (iBonus.utm_campaign) $("input[name=utm_campaign]").val(iBonus.utm_campaign);
if (iBonus.utm_term) $("input[name=utm_term]").val(iBonus.utm_term);
if (iBonus.utm_content) $("input[name=utm_content]").val(iBonus.utm_content);
if (iBonus.after_success_url) $("input[name=after_success_url]").val(iBonus.after_success_url);
if (iBonus.after_failure_url) $("input[name=after_failure_url]").val(iBonus.after_failure_url);

// !-----forms behaviour-----

// -----arrows click and scroll-----
$(".more-info-arrow-wrap").click(function() {
  $("html,body").animate(
    { scrollTop: $(".clients-base").offset().top },
    "slow"
  );
});
// !-----arrows click and scroll-----

////////////////////////////////////////////// i-Bonus scripts ///////////////////////////////////////////////////////////////////////////////////////////////
if (iBonus.status.toLowerCase() === "success") {
  $(".center-side").show();
  $(".alert-success").show(300);
}

if (iBonus.status.toLowerCase() === "failure") {
  $(".center-side").show();
  $(".alert-danger").show(300);
}

$(".form-wrapper .close").on("click", function() {
  $(".right-side").hide();
  $(".form-wrapper").hide(300);
});

$(".get-offer-button").on("click", function() {
  $(".right-side").show();
  $(".form-wrapper").show(300);
});

$(".alert .close, .center-side, .right-side").on("click", function() {
  $(".alert-success").hide(300);
  $(".form-wrapper").hide(300);
  $(".center-side").hide();
  $(".right-side").hide();
});

$(".alert, .form-wrapper").click(function(event) {
  event.stopPropagation();
});

$(document).keyup(function(e) {
  if (e.which == 27) {
    $(".alert-success").hide(300);
    $(".form-wrapper").hide(300);
    $(".center-side").hide();
    $(".right-side").hide();
  }
});
