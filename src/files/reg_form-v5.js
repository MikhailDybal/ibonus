(function($) {
    var rv_name = /^[a-zA-Zа-яА-Я0-9_\.\-\ ]+$/;
    var rv_email = /^([a-zA-Z0-9_\+\.\-])+\@(([a-zA-Z0-9_\.\-])+\.)+([a-zA-Z0-9]{2,6})+$/;
    var rv_phone = /^(\d|\+)[\d\(\)\ +-]{4,14}\d$/;

    $(document).ready(function() {
        forms_init();
        add_phone_mask();
    });
    
    var forms = $(".form-flex");

    function forms_init() {
        forms.each(function() {
            var fform = $(this);

            fform.bind("submit", function() {
                var name = fform.find('input[name="name"]');
                name.val($.trim(name.val()));
                var name_val = name.val();
                var inviter_name = fform.find('input[name="inviter_name"]');
                inviter_name.val($.trim(inviter_name.val()));
                var inviter_name_val = inviter_name.val();
                var phone = fform.find('input[name="phone"]');
                var phone_val = phone.val();
                var inviter_phone = fform.find('input[name="inviter_phone"]');
                var inviter_phone_val = inviter_phone.val();
                var email = fform.find('input[name="email"]');
                email.val($.trim(email.val()));
                var email_val = email.val();
                var validation = true;
        
                var phone_val_test = phone_val.replace("(", "");
                phone_val_test = phone_val_test.replace(")", "");
                phone_val_test = phone_val_test.replace("+", "");
                phone_val_test = phone_val_test.replace("-", "");
                phone_val_test = phone_val_test.replace("-", "");
                phone_val_test = phone_val_test.replace("-", "");
                phone_val_test = phone_val_test.replace(" ", "");
                phone_val_test = phone_val_test.replace(" ", "");
                phone_val_test = phone_val_test.replace(" ", "");
                phone_val_test = phone_val_test.replace(" ", "");
                // phone_val = phone_val_test;
        
                if (phone_val != "" && rv_phone.test(phone_val_test)) {
                    phone.removeClass("has-error");
                } else {
                    phone.addClass("has-error");
                    validation = false;
                }
        
                if (inviter_phone.length) {
                    var inviter_phone_val_test = inviter_phone_val.replace("(", "");
                    inviter_phone_val_test = inviter_phone_val_test.replace(")", "");
                    inviter_phone_val_test = inviter_phone_val_test.replace("+", "");
                    inviter_phone_val_test = inviter_phone_val_test.replace("-", "");
                    inviter_phone_val_test = inviter_phone_val_test.replace("-", "");
                    inviter_phone_val_test = inviter_phone_val_test.replace("-", "");
                    inviter_phone_val_test = inviter_phone_val_test.replace(" ", "");
                    inviter_phone_val_test = inviter_phone_val_test.replace(" ", "");
                    inviter_phone_val_test = inviter_phone_val_test.replace(" ", "");
                    inviter_phone_val_test = inviter_phone_val_test.replace(" ", "");
                  // inviter_phone_val = inviter_phone_val_test;
        
                    if (
                        inviter_phone_val != "" &&
                        rv_phone.test(inviter_phone_val_test)
                        ) {
                        inviter_phone.removeClass("has-error");
                    } else {
                        inviter_phone.addClass("has-error");
                        validation = false;
                    }
                }
        
                if (name_val.length > 2 && name_val != "" && rv_name.test(name_val)) {
                    name.removeClass("has-error");
                } else {
                    name.addClass("has-error");
                    validation = false;
                }
        
                if (inviter_name.length) {
                    if (
                        inviter_name_val.length > 2 &&
                        inviter_name_val != "" &&
                        rv_name.test(inviter_name_val)
                    ) {
                        inviter_name.removeClass("has-error");
                    } else {
                        inviter_name.addClass("has-error");
                        validation = false;
                    }
                }
        
                if (!fform.hasClass("email-required")) {
                    if (email.length) {
                        if (
                            (email_val != "" && rv_email.test(email_val)) ||
                            email_val == ""
                        ) {
                            if (email_val == "") {
                            email.addClass("has-error");
                            validation = false;
                            } else {
                            email.removeClass("has-error");
                            }
                        } else {
                            email.addClass("has-error");
                            validation = false;
                        }
                    }
                } else {
                    if (email_val != "" && rv_email.test(email_val)) {
                        email.removeClass("has-error");
                    } else {
                        email.addClass("has-error");
                        validation = false;
                    }
                }
        
                if (!validation) {
                    return false;
                }
            });
        });
    }
})(jQuery);

function add_phone_mask() {
    var phone = $("input[name=phone], .js-phone-mask");
    phone.each(function() {
        $(this).inputmask({
            alias: "phone"
        });
    });
}
