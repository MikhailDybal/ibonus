## Development build
* npm install
* npm run dev
* Go to: http://localhost:8080/

## Production build
* npm install
* npm run build
* Files to using: /files, *.html


Линк на макет
https://xd.adobe.com/spec/0ff85797-fe06-4d0f-5548-62e3bf82303f-79c9/

0. css, js - компановать в один файл.
css, js, картинки, видео залить в папку /files/ в корне (без подпапок).

1.
На месте 3х картинок разместить видео (см. архив анимация)


2.
Добавить 1 попапа сбоку справа для получить предложение, на кнопки "узнать подробнее". Как тут: https://xn--80aaa8afdnde1ba.xn--80adxhks/
Поля: телефон, имя, почта, кнопка отправить.


3. Обработка форм.
Чтобы было понятно зачем это все, механизм отправки такой: 0. При загрузке страницы подтягивать utm-метки из урла 1. валидация через js на клинете. 2. Отправка формы по form action атрибуту. 3. перенаправление на адрес указанный в хидден поле after_success_url или after_failure_url. 4. Показ попапа с информацией, удачная или неудачная отправка.

3.0. При загрузке страницы подтягивать utm метки
<input type="hidden" name="utm_source"/>
<input type="hidden" name="utm_medium"/>
<input type="hidden" name="utm_campaign"/>
<input type="hidden" name="utm_term"/>
<input type="hidden" name="utm_content"/>

3.1 Сделать валидацию полей форм на js (можно взять отсюда) https://xn--80aaa8afdnde1ba.xn--80adxhks/

3.2. 
<form action="https://sale-video.com/buy/41/" method="post" class="form-flex"> action оставляем пока что.

3.3. 
во всех трех формах оставляем 
<input type="hidden" name="after_success_url"
	value="https://xn--80aaa8afdnde1ba.xn--80adxhks/?status=success"/>
<input type="hidden" name="after_failure_url"
	value="https://xn--80aaa8afdnde1ba.xn--80adxhks/?status=failure"/>

3.4. 
Сделать в формах и в попапае сообщения нотификации: При открытии страницы с параметром, js-ом показывать попап с сообщением об успехе или неуспехе.
?status=success - открывать попап, сообщение успех
?status=failure - открывать попап, сообщение ошибка

Пример как здесь:
https://xn--80aaa8afdnde1ba.xn--80adxhks/?status=success
https://xn--80aaa8afdnde1ba.xn--80adxhks/?status=failure
Но покаызвать не попап с формой, а просто отдельный попап об успехе или отдельный попап об ошибке. Вне зависимости от того, с какой формы отправил чуви свои данные.


4. (Необязательно) Кнопка "вниз" - можешь заложить минимальную анимацию.


11.04.2019
Добавь, пожалуйста внизу страницы иконку эпстора, рядом со значком плеймаркета.
Сними кликабельность (ховер) с фразы "Цифровой помощник", Поставь только на икноки.
-Ссылок на приложения еще нет - поставь пока для гугл плея - play.google.com
-Для эпстора - appstore.com
-Цифровой помощник привязать к i-bonus.me/salesbot  